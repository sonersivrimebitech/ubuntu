FROM ubuntu:16.04

RUN apt-get update \
  && apt-get install curl wget unzip software-properties-common nano -y

CMD ["bash"]